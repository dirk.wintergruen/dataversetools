try:
    from dataverse import Connection
except:
    from dataverse.connection import Connection
from urllib.request import urlopen
from urllib.parse import urlencode
#ignore SSL check for the time beeing
import ssl
from os.path import split
import sys

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE
import logging

from fpdf import FPDF

class PDF(FPDF):
    def __init__(self):
        self.page_title = None
        super().__init__(orientation="P", unit="mm", format="A4")
        
    def header(self):
        # Logo
        #self.image('/Users/dwinter/Downloads/commission_all_offset_2.png', 10, 8, 33)
        # Arial bold 15
        self.set_font('Arial', 'B', 8)
        # Move to the right
        self.set_y(10)
        
        # Title
        if self.page_title is not None:
            self.cell(0, 100, self.page_title, 0, 0, 'C')
        if self.page_subtitle is not None:
            self.ln(20)
            self.cell(0, 10, self.page_subtitle, 0, 0, 'C')
        # Line break
        self.ln(20)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')

class DataverseError(Exception):
    def __init__(self, expression, message):
        self.expression = expression
        self.message = message
    
class DataverseHandler(object):
    """connects to a dataverse and provides a few convinient functions for uploading and downloading data"""
    
    def __init__(self,host,token,dataverse,use_https=True):
        """
        :param url host: Url of the dataverse host
        :param token host: api-toke at the dataverae
        :param dataverse str: name of the dataverse
        
        """
        self.connection = Connection(host, token,verify=False,use_https=use_https)
        self.dataverse= self.connection.get_dataverse(dataverse)
        
        assert self.dataverse != None, "Cannot open dataverse: %s"%dataverse
        self.token = token
    def getDataverse(self):
        return self.dataverse
        
    def getFileId(self,doi,filename,version="latest"):
        ds = self.dataverse.get_dataset_by_doi(doi)
       
        fl=ds.get_file(filename,version=version)
        fid = fl.id
        return fid
        
    def createPDFofAllImages(self,fn,dois=[],version="latest",max_pages=None):
        
        pdf = PDF()
        pdf.alias_nb_pages()
        cnt=1
        for ds in self.dataverse.get_datasets():
            title=ds.title
            
            if len(dois) > 0 and ds.doi not in dois:
                continue
           
            for f in ds.get_metadata(version=version)["files"]:
                print(cnt)
                if max_pages is not None and cnt > max_pages:
                    break
                
           
                f = f["dataFile"]
                try:    
                    fl=ds.get_file(f["filename"],version=version)
                    url = fl.download_url
                except:
                    logging.error("filename %snot found in %s."%(f,ds.doi))
                    continue
                
                if not f["contentType"].startswith("image"):
                    continue
                try:
                    cnt+=1
                    pdf.set_font('Times', '', 6)
                    pdf.page_title="%s"%(title)
                    print(title)
                    print(f["filename"])
                    pdf.page_subtitle=f["filename"]
                    pdf.add_page()
                    pdf.image(url+"?key="+self.token,type=f["contentType"].replace("image/",""),w=200)
                    
                    #pdf.title
                    
                    print(url)
                except:
                    print("Unexpected error:", sys.exc_info()[0],sys.exc_info()[1])
                    pass
        #pdf.add_page()
        pdf.output(fn, 'F')
                            
            
            
        
    def getContent(self,doi,filename,version="latest"):
        ds = self.dataverse.get_dataset_by_doi(doi)
        try:    
            fl=ds.get_file(filename,version=version)
            url = fl.download_url
        except:
            logging.error("filename %snot found in %s."%(filename,doi))
            return None
       
        resp = urlopen(url+"?"+urlencode({"key":self.token,"format":"original"}),context=ctx)
        return resp
        
    def replaceOrCreateFile(self,ds,filename,dataverseFilename=None,onlyName=True):
        if dataverseFilename is None:
            dataverseFilename=filename
            if onlyName:
                p,dataverseFilename=split(dataverseFilename)
        
        
        
        fl = ds.get_file(dataverseFilename)
        if fl is not None:
            ds.delete_file(fl)
        ds.upload_file(dataverseFilename,open(filename,"rb").read())
    #def storeContent(self):
        
    def getOrCreateDataset(self,title,creator,description):
        if description is None or description=="":
            raise DataverseError("getOrCreateDataset","description cannot be empty")
        
        dv=self.dataverse
        newDS = dv.get_dataset_by_title(title)
        if newDS is None:
            newDS=dv.create_dataset(title=title,creator=creator,description=description)
        return newDS